import java.io.FileInputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.HashMap;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.server.handler.ContextHandler;

import com.futurae.futuraeweb.FuturaeWeb;


public class FuturaeExampleServer {

  public static void main(String[] args) throws Exception {
    Server server = new Server(8080);

    ContextHandler contextHandler = createJsFileHandler();
    ServletHandler servletHandler = new ServletHandler();
    servletHandler.addServletWithMapping(FuturaeFrameServlet.class, "/*");
    HandlerList handlers = new HandlerList();
    handlers.setHandlers(new Handler[] {contextHandler, servletHandler});
    server.setHandler(handlers);

    server.start();
    server.join();
  }

  private static ContextHandler createJsFileHandler() {
    ResourceHandler resourceHander = new ResourceHandler();
    resourceHander.setResourceBase("js");
    ContextHandler contextHandler = new ContextHandler("/js");
    contextHandler.setHandler(resourceHander);

    return contextHandler;
  }

  public static final class FuturaeFrameServlet extends HttpServlet {
    private static final String USERNAME_PARAM = "username";
    private static final String RESPONSE_PARAM = "sig_response";

    // Properties file creds
    private static final String HOST = "host";
    private static final String WID = "wid";
    private static final String WKEY = "wkey";
    private static final String SKEY = "skey";

    private Properties futuraeProperties;

    @Override
    public void init() throws ServletException {
      try {
        futuraeProperties = getFuturaeProperties();
      } catch (Exception e) {
        throw new ServletException(e);
      }
    }

    @Override
    public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
      String username = request.getParameter(USERNAME_PARAM);
      if (username == null) {
        serveContent("<h1>Provide a Futurae username in the query params, e.g. http://localhost:8080/?" + USERNAME_PARAM + "=futuraeusername</h1>", HttpServletResponse.SC_OK, response);
        return;
      }

      try {
        serveFuturaeFrame(username, response);
      } catch (Exception e) {
        serveContent("<h1>Error: " + e.getMessage() + "</h1>", HttpServletResponse.SC_INTERNAL_SERVER_ERROR, response);
      }
    }

    @Override
    public void doPost(final HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      String futuraeResponse = request.getParameter(RESPONSE_PARAM);
      if (futuraeResponse == null) {
        serveContent("<h1>Unexpected request - this app only expects POSTs from the Futurae Widget</h1>", HttpServletResponse.SC_BAD_REQUEST, response);
        return;
      }

      String wid = futuraeProperties.getProperty(WID);
      String wkey = futuraeProperties.getProperty(WKEY);
      String skey = futuraeProperties.getProperty(SKEY);

      try {
        String username = FuturaeWeb.verifyResponse(wid, wkey, skey, futuraeResponse);
        serveContent("<h1>You have been succefully authenticated with Futurae!</h1>", HttpServletResponse.SC_OK, response);
      } catch (Exception e) {
        serveContent("<h1>Error: " + e.getMessage() + "</h1>", HttpServletResponse.SC_INTERNAL_SERVER_ERROR, response);
      }
    }

    private static Properties getFuturaeProperties() throws FileNotFoundException, IOException, FuturaePropertyException {
      Properties futuraeProperties = new Properties();
      futuraeProperties.load(new FileInputStream("futurae.properties"));
      if (!futuraeProperties.containsKey(WID)) {
        throw new FuturaePropertyException("wid is a required property");
      }
      if (!futuraeProperties.containsKey(WKEY)) {
        throw new FuturaePropertyException("wkey is a required property");
      }
      if (!futuraeProperties.containsKey(SKEY)) {
        throw new FuturaePropertyException("skey is a required property");
      }
      if (!futuraeProperties.containsKey(HOST)) {
        throw new FuturaePropertyException("host is a required property");
      }
      return futuraeProperties;
    }

    private static void serveContent(final String content, final int status, final HttpServletResponse response) throws IOException {
      response.setContentType("text/html; charset=utf-8");
      response.setStatus(status);
      response.getWriter().println(content);
    }

    private void serveFuturaeFrame(final String username, final HttpServletResponse response) throws IOException {
      String wid = futuraeProperties.getProperty(WID);
      String wkey = futuraeProperties.getProperty(WKEY);
      String skey = futuraeProperties.getProperty(SKEY);

      String request = FuturaeWeb.signRequest(wid, wkey, skey, username);
      if (request.startsWith("ERR|")) {
        serveContent("<h1>Error, bad request: " + request + "</h1>", HttpServletResponse.SC_INTERNAL_SERVER_ERROR, response);
        return;
      }

      String futuraeHost = futuraeProperties.getProperty(HOST);
      String frameHtml = getFramePage(futuraeHost, request);
      serveContent(frameHtml, HttpServletResponse.SC_OK, response);
    }

    private static String getFramePage(final String host, final String request) {
      String eol = System.getProperty("line.separator");
      String framePage =
        "<!DOCTYPE html>" + eol +
        "<html>" + eol +
        "  <head>" + eol +
        "    <title>Futurae Web Widget</title>" + eol +
        "    <meta name='viewport' content='width=device-width, initial-scale=1'>" + eol +
        "  </head>" + eol +
        "  <body>" + eol +
        "    <h1>Futurae Web Widget</h1>" + eol +
        "    <script src='/js/Futurae-Web-SDK-v1.js'></script>" + eol +
        "    <iframe style='width: 800px; height: 501px;' allow='microphone' id='futurae_widget'" + eol +
        "            title='2-Factor Authentication'" + eol +
        "            frameborder='0'" + eol +
        "            data-host='" + host + "'" + eol +
        "            data-sig-request='" + request + "'" + eol +
        "            data-lang='en'" + eol +
        "    </iframe>" + eol +
        "  </body>" + eol +
        "</html>";

        return framePage;
    }
  }

  public static final class FuturaePropertyException extends Exception {
    public FuturaePropertyException(String message) {
      super(message);
    }
  }
}
