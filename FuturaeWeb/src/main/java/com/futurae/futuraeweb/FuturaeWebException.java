package com.futurae.futuraeweb;

public class FuturaeWebException extends Exception {

  public FuturaeWebException(String message) {
    super(message);
  }
}
