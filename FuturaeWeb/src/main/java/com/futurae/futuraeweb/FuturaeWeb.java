package com.futurae.futuraeweb;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public final class FuturaeWeb {
	private static final String REQUEST_PREFIX = "REQ";
	private static final String APP_PREFIX = "APP";
	private static final String AUTH_PREFIX = "AUTH";

	private static final int FUTURAE_EXPIRY = 600;
	private static final int APP_EXPIRY = 3600;

	private static final int WID_LEN = 36;
	private static final int WKEY_LEN = 40;
	private static final int SKEY_LEN = 40;

	public static final String ERR_USERNAME = "ERR|The username passed to sign_request() is invalid.";
	public static final String ERR_WID = "ERR|The Futurae Web ID passed to sign_request() is invalid.";
	public static final String ERR_WKEY = "ERR|The Futurae Web Key passed to sign_request() is invalid.";
	public static final String ERR_SKEY = "ERR|The Secret Key passed to sign_request() must be at least " + SKEY_LEN + " characters.";
	public static final String ERR_UNKNOWN = "ERR|An unknown error has occurred.";

	public static String signRequest(final String wid, final String wkey, final String skey, final String username) {
    final long ts = System.currentTimeMillis() / 1000;
		final String sig;
		final String app_sig;

		if (username.equals("") || username.indexOf('|') != -1) {
			return ERR_USERNAME;
		}
		if (wid.equals("") || wid.length() != WID_LEN) {
			return ERR_WID;
		}
		if (wkey.equals("") || wkey.length() < WKEY_LEN) {
			return ERR_WKEY;
		}
		if (skey.equals("") || skey.length() < SKEY_LEN) {
			return ERR_SKEY;
		}

		try {
			sig = signVals(wkey, username, wid, REQUEST_PREFIX, FUTURAE_EXPIRY, ts);
			app_sig = signVals(skey, username, wid, APP_PREFIX, APP_EXPIRY, ts);
		} catch (Exception e) {
			return ERR_UNKNOWN;
		}

		return sig + ":" + app_sig;
	}

	public static String verifyResponse(final String wid, final String wkey, final String skey, final String sig_response)
		throws FuturaeWebException, NoSuchAlgorithmException, InvalidKeyException, IOException {

		String auth_user = null;
		String app_user = null;

    final long ts = System.currentTimeMillis() / 1000;
		final String[] sigs = sig_response.split(":");
		final String auth_sig = sigs[0];
		final String app_sig = sigs[1];

		auth_user = parseVals(wkey, auth_sig, AUTH_PREFIX, wid, ts);
		app_user = parseVals(skey, app_sig, APP_PREFIX, wid, ts);

		if (!auth_user.equals(app_user)) {
			throw new FuturaeWebException("Authentication failed.");
		}

		return auth_user;
	}

	private static String signVals(final String key, final String username, final String wid, final String prefix, final int expire, final long time)
		throws InvalidKeyException, NoSuchAlgorithmException {
		final long expire_ts = time + expire;
		final String exp = Long.toString(expire_ts);

		final String val = wid + "|" + username + "|" + exp;
		final String cookie = prefix + "|" + Base64.getEncoder().encodeToString(val.getBytes());
		final String sig = hmacSign(key, cookie);

		return cookie + "|" + sig;
	}

	private static String parseVals(final String key, final String val, final String prefix, final String wid, final long time)
		throws InvalidKeyException, NoSuchAlgorithmException, IOException, FuturaeWebException {

		final String[] parts = val.split("\\|");
		if (parts.length != 3) {
			throw new FuturaeWebException("Invalid response");
		}

		final String u_prefix = parts[0];
		final String u_b64 = parts[1];
		final String u_sig = parts[2];

		final String sig = hmacSign(key, u_prefix + "|" + u_b64);
		if (!hmacSign(key, sig).equals(hmacSign(key, u_sig))) {
			throw new FuturaeWebException("Invalid response");
		}

		if (!u_prefix.equals(prefix)) {
			throw new FuturaeWebException("Invalid response");
		}

		final byte[] decoded = Base64.getDecoder().decode(u_b64);
		final String cookie = new String(decoded);

		final String[] cookie_parts = cookie.split("\\|");
		if (cookie_parts.length != 3) {
			throw new FuturaeWebException("Invalid response");
		}
		final String u_wid = cookie_parts[0];
		final String username = cookie_parts[1];
		final String expiry = cookie_parts[2];

		if (!u_wid.equals(wid)) {
			throw new FuturaeWebException("Invalid response");
		}

		final long expiry_ts = Long.parseLong(expiry);
		if (time >= expiry_ts) {
			throw new FuturaeWebException("Request has expired. Please check that the system time is correct.");
		}

		return username;
	}

  private static String hmacSign(String skey, String data)
			throws NoSuchAlgorithmException, InvalidKeyException {
		SecretKeySpec key = new SecretKeySpec(skey.getBytes(), "HmacSHA256");
		Mac mac = Mac.getInstance("HmacSHA256");
		mac.init(key);
		byte[] raw = mac.doFinal(data.getBytes());
		return bytesToHex(raw);
	}

	private static String bytesToHex(byte[] b) {
		String result = "";
		for (int i = 0; i < b.length; i++) {
			result += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
		}
		return result;
	}
}
