package com.futurae.futuraeweb;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import org.junit.Test;

import static org.junit.Assert.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class FuturaeWebTest {

  private static final String WID = "FIDXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX";
	private static final String WRONG_WID = "FIXXXXXXXXXXXXXXXXXY";
  private static final String WKEY = "futuraegeneratedsharedwebapplicationkey.";
  private static final String SKEY = "useselfgeneratedhostapplicationsecretkey";
	private static final String USERNAME = "testusername";

	@Test
	public void testSignRequest_whenValidParameters() {
		String sig = FuturaeWeb.signRequest(WID, WKEY, SKEY, USERNAME);
		assertNotNull(sig);
	}

	@Test
	public void testSignRequest_whenEmptyUsername() {
		String sig = FuturaeWeb.signRequest(WID, WKEY, SKEY, "");
		assertEquals(sig, FuturaeWeb.ERR_USERNAME);
	}

	@Test
	public void testSignRequest_whenInvalidUsername() {
		String sig = FuturaeWeb.signRequest(WID, WKEY, SKEY, "in|valid");
		assertEquals(sig, FuturaeWeb.ERR_USERNAME);
	}

	@Test
	public void testSignRequest_whenInvalidWid() {
		String sig = FuturaeWeb.signRequest("invalid", WKEY, SKEY, USERNAME);
		assertEquals(sig, FuturaeWeb.ERR_WID);
	}

	@Test
	public void testSignRequest_whenInvalidWkey() {
		String sig = FuturaeWeb.signRequest(WID, "invalid", SKEY, USERNAME);
		assertEquals(sig, FuturaeWeb.ERR_WKEY);
	}

	@Test
	public void testSignRequest_whenInvalidSkey() {
		String sig = FuturaeWeb.signRequest(WID, WKEY, "invalid", USERNAME);
		assertEquals(sig, FuturaeWeb.ERR_SKEY);
	}

	@Test public void testVerifyResponse_whenInvalidUsername() {
		String[] sigs;
		String sig;
		String valid_app_sig;
		String invalid_user;

		sig = FuturaeWeb.signRequest(WID, WKEY, SKEY, USERNAME);
		sigs = sig.split(":");
		valid_app_sig = sigs[1];

		try {
			invalid_user = FuturaeWeb.verifyResponse(WID, WKEY, SKEY, Helpers.invalidResponse() + ":" + valid_app_sig);
			fail();
		} catch (Exception e) {

		}
	}

	@Test public void testVerifyResponse_whenExpiredResponse() {
		String[] sigs;
		String sig;
		String valid_app_sig;
		String expired_user, expired_response;

		sig = FuturaeWeb.signRequest(WID, WKEY, SKEY, USERNAME);
		sigs = sig.split(":");
		valid_app_sig = sigs[1];

		try {
      expired_response = Helpers.expiredResponse(WID, WKEY, USERNAME);

			expired_user = FuturaeWeb.verifyResponse(WID, WKEY, SKEY, expired_response + ":" + valid_app_sig);
			fail();
		} catch (Exception e) {

		}
	}

	@Test public void testVerifyResponse_whenInvalidAppSig() {
		String[] sigs;
		String sig;
		String invalid_app_sig;
		String future_response, future_user;

		sig = FuturaeWeb.signRequest(WID, WKEY, "invalidinvalidinvalidinvalidinvalidinvalid", USERNAME);
		sigs = sig.split(":");
		invalid_app_sig = sigs[1];

		try {
      future_response = Helpers.futureResponse(WID, WKEY, USERNAME);

			future_user = FuturaeWeb.verifyResponse(WID, WKEY, SKEY, future_response + ":" + invalid_app_sig);
			fail();
		} catch (Exception e) {

		}
	}

	@Test public void testVerifyResponse_whenValid() {
		String[] sigs;
		String sig;
		String valid_app_sig;
		String future_response, future_user;

		sig = FuturaeWeb.signRequest(WID, WKEY, SKEY, USERNAME);
		sigs = sig.split(":");
		valid_app_sig = sigs[1];

		try {
      future_response = Helpers.futureResponse(WID, WKEY, USERNAME);

			future_user = FuturaeWeb.verifyResponse(WID, WKEY, SKEY, future_response + ":" + valid_app_sig);
			assertEquals(future_user, USERNAME);
		} catch (Exception e) {
			fail();
		}
	}

	@Test public void testVerifyResponse_whenInvalidResponseParameters() {
		String[] sigs;
		String sig;
		String valid_app_sig;
		String bad_params_response, future_user;

		sig = FuturaeWeb.signRequest(WID, WKEY, SKEY, USERNAME);
		sigs = sig.split(":");
		valid_app_sig = sigs[1];

		try {
      bad_params_response = Helpers.badParamsResponse(WID, WKEY, USERNAME);

			future_user = FuturaeWeb.verifyResponse(WID, WKEY, SKEY, bad_params_response + ":" + valid_app_sig);
			fail();
		} catch (Exception e) {

		}
	}

	@Test public void testVerifyResponse_whenInvalidAppParameters() {
		String[] sigs;
		String sig;
		String bad_params_app;
		String future_response, future_user;

		try {
      bad_params_app = Helpers.badParamsApp(WID, SKEY, USERNAME);
      future_response = Helpers.futureResponse(WID, WKEY, USERNAME);

      future_user = FuturaeWeb.verifyResponse(WID, WKEY, SKEY, future_response + ":" + bad_params_app);
      fail();
		} catch (Exception e) {

		}
	}

	@Test public void testVerifyResponse_whenInvalidWid() {
		String[] sigs;
		String sig;
		String valid_app_sig;
		String future_response, future_user;

		sig = FuturaeWeb.signRequest(WID, WKEY, SKEY, USERNAME);
		sigs = sig.split(":");
		valid_app_sig = sigs[1];

		try {
      future_response = Helpers.futureResponse(WID, WKEY, USERNAME);

			future_user = FuturaeWeb.verifyResponse(WRONG_WID, WKEY, SKEY, future_response + ":" + valid_app_sig);
			fail();
		} catch (Exception e) {

		}
	}
}
