package com.futurae.futuraeweb;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;


public class Helpers {
  public static String badParamsApp(final String wid, final String skey, final String username)
			throws NoSuchAlgorithmException, InvalidKeyException {
		final String val = wid + "|" + username + "|" + expiredExpiry() + "|" + "bad_param";
		final String cookie = "APP|" + Base64.getEncoder().encodeToString(val.getBytes());
		final String sig = hmacSign(skey, cookie);
    return cookie + "|" + sig;
  }

  public static String badParamsResponse(final String wid, final String wkey, final String username)
			throws NoSuchAlgorithmException, InvalidKeyException {
		final String val = wid + "|" + username + "|" + expiredExpiry() + "|" + "bad_param";
		final String cookie = "AUTH|" + Base64.getEncoder().encodeToString(val.getBytes());
		final String sig = hmacSign(wkey, cookie);
    return cookie + "|" + sig;
  }

  public static String expiredResponse(final String wid, final String wkey, final String username)
			throws NoSuchAlgorithmException, InvalidKeyException {
		final String val = wid + "|" + username + "|" + expiredExpiry();
		final String cookie = "AUTH|" + Base64.getEncoder().encodeToString(val.getBytes());
		final String sig = hmacSign(wkey, cookie);
    return cookie + "|" + sig;
  }

  public static String futureResponse(final String wid, final String wkey, final String username)
			throws NoSuchAlgorithmException, InvalidKeyException {
		final String val = wid + "|" + username + "|" + futureExpiry();
		final String cookie = "AUTH|" + Base64.getEncoder().encodeToString(val.getBytes());
		final String sig = hmacSign(wkey, cookie);
    return cookie + "|" + sig;
  }

  public static String invalidResponse() {
    return "AUTH|INVALID|SIG";
  }

  private static String futureExpiry() {
    return Long.toString(System.currentTimeMillis() / 1000 + 600);
  }

  private static String expiredExpiry() {
    return Long.toString(System.currentTimeMillis() / 1000 - 600);
  }

  private static String hmacSign(String skey, String data)
			throws NoSuchAlgorithmException, InvalidKeyException {
		SecretKeySpec key = new SecretKeySpec(skey.getBytes(), "HmacSHA256");
		Mac mac = Mac.getInstance("HmacSHA256");
		mac.init(key);
		byte[] raw = mac.doFinal(data.getBytes());
		return bytesToHex(raw);
	}

	private static String bytesToHex(byte[] b) {
		String result = "";
		for (int i = 0; i < b.length; i++) {
			result += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
		}
		return result;
	}
}
