## Overview

**futurae-web-java** - Provides the Futurae Web JAVA helpers to be integrated in your JAVA web
application.

These helpers allow developers to integrate Futurae's Web authentication suite into their web apps,
without the need to implement the Futurae Auth API.

## Build and Run Unit Tests

```bash
cd ./FuturaeWeb/ && mvn package
```

## Example

The dependencies are included in the example. You just have to fill in your Futurae Web Widget
credentials in the `futurae.properties` file and then:

```bash
$ cd ./example
$ javac -cp "lib/*" FuturaeExampleServer.java
$ java -cp "lib/*:." FuturaeExampleServer
```

You can then visit `http://localhost:8080/?username=ausername` and check the Futurae Web Widget
integration.

## Documentation

Documentation on using the Futurae Web Widget can be found [here](https://futurae.com/docs/guide/futurae-web/).
